#Save directory of the script in variable
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
USER_HOME=$(getent passwd ${SUDO_USER:-$USER} | cut -d: -f6)
ORIGINAL_USER=${SUDO_USER:-$USER}

#Download and install K3s, then create a single node cluster
curl -sfL https://get.k3s.io | sh -

#Wait for availability of Traefik-CRDs in the cluster
counter=0
retries=15
sleeptime=10
while [ $counter -le $retries ]
do
   ((counter++))
   kubectl get ingressroute
   if [ $? -eq 0 ]; then
      break
   else
      if [ $counter -eq $retries ]; then
         echo CRDs are not yet available, please install helm-chart manually
         exit 1
      fi
      echo CRDs not yet ready, retrying in $sleeptime seconds. \(Try $counter/$retries\)
      sleep $sleeptime
   fi
done

#Edit K3s service environment variable file to change access permission to k3s.yaml if not root
if [ $ORIGINAL_USER != 'root' ]; then
   echo User not root, changing access permission for k3s.yaml
   sudo echo K3S_KUBECONFIG_MODE=\"644\" >> /etc/systemd/system/k3s.service.env
   sudo systemctl restart k3s
fi

#Download Linkerd and install onto cluster
linkerd version
if [ $? -eq 0 ]; then
   echo Linkerd already installed
else
   curl --proto '=https' --tlsv1.2 -sSfL https://run.linkerd.io/install | sudo -u $ORIGINAL_USER sh
fi

export PATH=$PATH:$USER_HOME/.linkerd2/bin
export KUBECONFIG=/etc/rancher/k3s/k3s.yaml

linkerd install --crds | kubectl apply -f -
linkerd install | kubectl apply -f -

#Install Helm if not present
helm version
if [ $? -eq 0 ]; then
   echo Helm already installed
else
   curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
   sudo apt-get install apt-transport-https --yes
   echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
   sudo apt-get update
   sudo apt-get install helm
fi

#Wait for availability of Linkerd
counter=0
retries=3
sleeptime=10
while [ $counter -le $retries ]
do
   ((counter++))
   echo checking Linkerd status...
   linkerd check
   if [ $? -eq 0 ]; then
      break
   else
      if [ $counter -eq $retries ]; then
         echo Linkerd is not yet ready, check if it was installed correctly or try installing it manually
         exit 1
      fi
      echo Linkerd not yet ready, retrying in $sleeptime seconds. \(Try $counter/$retries\)
      sleep $sleeptime
   fi
done

#Install Helm-Chart
helm install helm-msa $SCRIPT_DIR/helm/helm-msa

#Edit Traefik-Deployment Configuration to allow cross-namespace access
kubectl get deploy traefik -n kube-system -o yaml > traefik.yaml
if ! grep -q "allowCrossNamespace=true" "traefik.yaml"; then
   sed -i '/providers.kubernetescrd/ a\ \ \ \ \ \ \ \ - --providers.kubernetescrd.allowCrossNamespace=true' traefik.yaml
   kubectl replace -f traefik.yaml
fi
rm traefik.yaml
