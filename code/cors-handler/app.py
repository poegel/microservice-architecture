import os

from flask import Flask
from flask import Response, request
from flask_cors import CORS
#import hashlib
import logging

app = Flask(__name__)
#CORS(app, expose_headers='*', origins=['http://134.103.212.72:30001', 'http://localhost:8080', "null"], supports_credentials=True)
CORS(app, expose_headers='*', origins=[os.getenv('ALLOWED_ORIGINS', "*")], supports_credentials=os.getenv('SUPPORTS_CREDENTIALS', True), allow_headers=os.getenv('ALLOW_HEADERS', "authorization"), methods=[os.getenv('ALLOW_METHODS', "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT")])

#@app.before_request
#def basic_authentication():
#    if request.method.lower() == 'options':
#        response = Response(status=200)
#        response.headers["Access-Control-Allow-Credentials"] = os.getenv('ALLOW_CREDENTIALS', True)
#        response.headers["Access-Control-Allow-Headers"] = os.getenv('ALLOW_HEADERS', "authorization")
#        response.headers["Access-Control-Allow-Methods"] = os.getenv('ALLOW_METHODS', "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT")
#        response.headers["Access-Control-Allow-Origin"] = os.getenv('ALLOW_ORIGIN', "*")
#        response.headers["Access-Control-Expose-Headers"] = os.getenv('EXPOSE_HEADERS', "*")
#        return response

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return 'Handling OPTIONS request for path: %s' % path

if __name__ == '__main__':
    app.logger.setLevel(logging.DEBUG)
    app.run(host=os.getenv('HOST_ADDRESS', "0.0.0.0"), port=5000)
