import os

from flask import Flask
from flask import Response, request
from flask_cors import CORS
import hashlib
import logging

import authModel

app = Flask(__name__)
CORS(app, expose_headers='*', origins=[os.getenv('ALLOWED_ORIGINS', "*")], supports_credentials=os.getenv('SUPPORTS_CREDENTIALS', True), allow_headers=os.getenv('ALLOW_HEADERS', "authorization"), methods=[os.getenv('ALLOW_METHODS', "DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT")])

# API Route for checking the client_id and client_secret
@app.route("/login", methods=["GET", "POST"])
def login():
    # get the username and password from the client application
    username = request.form.get("user")
    password = request.form.get("password")

    # the password in the database is "hashed" with a one-way hash
    hash_object = hashlib.sha1(bytes(password, 'utf-8'))
    hashed_password = hash_object.hexdigest()

    # make a call to the model to authenticate
    authentication = authModel.authenticate(username, hashed_password)
    if authentication == False:
        return Response("Authentication unsuccessful", status=401, mimetype='application/json')
    else:
        response = Response("Authentication successful", status=200, mimetype='application/json')
        response.headers["UserID"] = username
        response.headers["JWT"] = authentication["token"]
        response.headers["IsAdmin"] = authentication["isAdmin"]
        response.headers["UserRoles"] = authentication["roles"]
        return response

# API route for verifying the token passed by API calls
@app.route("/verify", methods=["GET", "POST"])
def verify():
    # Extract and verify the JWT-token
    token = ""
    authorizationHeader = request.headers.get('authorization')
    print("AuthorizationHeader: "+authorizationHeader)
    if authorizationHeader is not None:
        token = authorizationHeader.replace("Bearer ", "")
    verification = authModel.verify(token)
    if not verification:
        return Response("Verification unsuccessful", status=401, mimetype='application/json')
    else:
        response = Response("Authentication successful", status=200, mimetype='application/json')
        response.headers["UserID"] = verification["username"]
        response.headers["JWT"] = token
        response.headers["IsAdmin"] = verification["isAdmin"]
        response.headers["UserRoles"] = verification["roles"]
        return response

@app.route("/logout", methods=["GET", "POST"])
def logout():
    #token = request.form.get("token")
    authorizationHeader = request.headers.get('authorization')
    token = authorizationHeader.replace("Bearer ", "")
    status = authModel.blacklist(token)
    if status:
        return Response("Logout successful", status=200, mimetype='application/json')
    else:
        return Response("Logout unsuccessful", status=409, mimetype='application/json')


@app.route("/user", methods=["POST", "DELETE"])
def user():
    if request.method == 'POST':

        is_admin = False

        # verify the token
        token = ""
        authorizationHeader = request.headers.get('authorization')
        if authorizationHeader is not None:
            token = authorizationHeader.replace("Bearer ", "")
        if authModel.verify(token):
            if not authModel.isAdmin(token):
                if authModel.admin_exists():
                    return Response("Registration unsuccessful", status=403, mimetype='application/json')
                else:
                    # If there's no admin user already, the first created users becomes an admin
                    is_admin = True
        else:
            if authModel.admin_exists():
                return Response("Registration unsuccessful", status=401, mimetype='application/json')
            else:
                # If there's no admin user already, the first created users becomes an admin
                is_admin = True

        # get the username and password from the client application
        username = request.form.get("user")
        password = request.form.get("password")
        roles = request.form.get("roles")

        # the password in the database is hashed with a one-way hash
        hash_object = hashlib.sha1(bytes(password, 'utf-8'))
        hashed_password = hash_object.hexdigest()

        # make a call to the model to authenticate
        createResponse = authModel.create(username, hashed_password, is_admin, roles)

        if createResponse:
            return Response("Registration successful", status=200, mimetype='application/json')
        else:
            return Response("Registration unsuccessful", status=409, mimetype='application/json')

    elif request.method == 'DELETE':
        username = request.form.get("user")

        # verify the token
        token = ""
        authorizationHeader = request.headers.get('authorization')
        if authorizationHeader is not None:
            token = authorizationHeader.replace("Bearer ", "")
        decoded_token = authModel.verify(token)
        if decoded_token:
            if not decoded_token.get("isAdmin") and not decoded_token.get("username") == username:
                return Response("Deletion unsuccessful", status=403, mimetype='application/json')
        else:
            return Response("Deletion unsuccessful", status=401, mimetype='application/json')

        delete_success = authModel.delete(username)

        if delete_success:
            # invalidate JWT token if user deleted his account
            if decoded_token.get("username") == username:
                authModel.blacklist(token)
            return Response("Deletion successful", status=200, mimetype='application/json')
        else:
            return Response("Deletion unsuccessful", status=409, mimetype='application/json')
    else:
        return Response("Deletion unsuccessful", status=409, mimetype='application/json')


@app.route("/blacklist/cleanup", methods=["POST"])
def cleanupBlacklist():
    # verify the token
    token = ""
    authorizationHeader = request.headers.get('authorization')
    if authorizationHeader is not None:
        token = authorizationHeader.replace("Bearer ", "")
    if authModel.verify(token):
        if not authModel.isAdmin(token):
            return Response("Blacklist cleanup unsuccessful", status=403, mimetype='application/json')
    else:
        return Response("Blacklist cleanup unsuccessful", status=401, mimetype='application/json')

    if authModel.cleanBlacklist():
        return Response("Blacklist cleanup successful", status=200, mimetype='application/json')
    else:
        return Response("Deletion unsuccessful", status=409, mimetype='application/json')


@app.route('/health')
def health():
    return "I'm alive"


if __name__ == '__main__':
    app.logger.setLevel(logging.DEBUG)
    authModel.db_setup()
    #app.run(host='0.0.0.0', port=5000, debug=True)
    app.run(host=os.getenv('HOST_ADDRESS'), port=5000)
