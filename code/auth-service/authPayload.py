from datetime import datetime
from datetime import timedelta
import os

class authPayload(dict):

    def __init__(self, id, username, isAdmin, roles):

        EXPIRESSECONDS = int(os.getenv('EXPIRES_SECONDS'))

        # The id of the user in the database
        self.id = id

        # The username of the client
        self.username = username

        # Admin status of the user
        self.isAdmin = isAdmin

        # roles of the user
        self.roles = roles

        # set the expiry attribute
        self.exp = datetime.utcnow() + timedelta(seconds=EXPIRESSECONDS)