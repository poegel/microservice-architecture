import os

# pip install psycopg2
import psycopg2

# pip install pyjwt
import jwt
import traceback

from datetime import datetime
from app import app
from authPayload import authPayload
from authResponse import authResponse

# Get environment variables
DBNAME = os.getenv('DATABASE_NAME')
DBUSER = os.getenv('DATABASE_USER')
DBPASSWORD = os.getenv("DATABASE_PASSWORD")
DBHOST = os.getenv("DATABASE_HOST")
DBPORT = os.getenv("DATABASE_PORT")
AUTHSECRET = os.getenv("AUTH_SECRET")
EXPIRESSECONDS = os.getenv('EXPIRES_SECONDS')


def authenticate(username, password):
    conn = None
    try:
        conn, cur = db_connect()
        cur.execute("SELECT * FROM users WHERE username LIKE %s AND password LIKE %s;", (username, password))
        rows = cur.fetchall()
        isAdmin = False

        if cur.rowcount == 1:
            for row in rows:
                user_id = row[0]
                username = row[1]
                isAdmin = row[3]
                break

            cur.execute("SELECT * FROM roles WHERE user_id = %s;", (user_id,))
            roles = []
            rows = cur.fetchall()
            for row in rows:
                roles.append(row[2])

            payload = authPayload(user_id, username, isAdmin, roles)

            encoded_jwt = jwt.encode(payload.__dict__, AUTHSECRET, algorithm='HS256')
            response = authResponse(encoded_jwt, EXPIRESSECONDS, isAdmin, roles)

            return response.__dict__
        else:
            return False

    except (Exception, psycopg2.DatabaseError) as error:

        app.logger.error(error)
        traceback.print_exc()
        if conn is not None:
            cur.close()
            conn.close()

        return False
    finally:
        if conn is not None:
            cur.close()
            conn.close()


def admin_exists():
    conn = None
    try:
        conn, cur = db_connect()
        cur.execute("SELECT * FROM users WHERE isadmin = %s;", (True,))
        cur.fetchall()
        if cur.rowcount >= 1:
            return True
        else:
            return False
    except (Exception, psycopg2.DatabaseError) as error:

        app.logger.error(error)
        traceback.print_exc()
        if conn is not None:
            cur.close()
            conn.close()

        return False
    finally:
        if conn is not None:
            cur.close()
            conn.close()


def verify(token):
    try:
        isBlacklisted = checkBlacklist(token)
        if isBlacklisted == True:
            return False
        else:
            # Also checks if token is expired
            decoded = jwt.decode(token, AUTHSECRET, algorithms=['HS256'])
            return decoded
    except Exception as error:
        app.logger.error(error)
        traceback.print_exc()
        return False


def getTokenClaims(token):
    try:
        decoded = jwt.decode(token, AUTHSECRET, algorithms=['HS256'])
        username = decoded.get("username")
        isAdmin = decoded.get("isAdmin")
        return username, isAdmin
    except Exception as error:
        app.logger.error(error)
        return False


def isAdmin(token):
    try:
        decoded = jwt.decode(token, AUTHSECRET, algorithms=['HS256'])
        isAdmin = decoded.get("isAdmin")
        return isAdmin
    except Exception as error:
        app.logger.error(error)
        return False


def create(username, password, is_admin, roles_str):
    conn = None
    # Split roles into list
    roles = []
    if roles_str is not None:
        roles = roles_str.replace(" ", "").split(",")

    try:
        conn, cur = db_connect()
        cur.execute("INSERT INTO users (username, password, isAdmin) VALUES(%s, %s, %s) RETURNING id;", (username, password, is_admin))
        user_id = cur.fetchone()[0]
        for role in roles:
            cur.execute("INSERT INTO roles (user_id, role) VALUES(%s, %s);", (user_id, role))
        conn.commit()
        return True
    except (Exception, psycopg2.DatabaseError) as error:
        app.logger.error(error)
        if conn is not None:
            cur.close()
            conn.close()

        return False
    finally:
        if conn is not None:
            cur.close()
            conn.close()


def delete(username):
    conn = None

    try:
        conn, cur = db_connect()
        cur.execute("DELETE FROM users WHERE username LIKE %s", (username,))
        conn.commit()
        return True

    except (Exception, psycopg2.DatabaseError) as error:
        app.logger.error(error)
        if conn is not None:
            cur.close()
            conn.close()

        return False
    finally:
        if conn is not None:
            cur.close()
            conn.close()


def blacklist(token):
    conn = None

    try:
        conn, cur = db_connect()
        cur.execute("INSERT INTO blacklist (token) VALUES(%s);", (token, ))
        conn.commit()
        return True
    except (Exception, psycopg2.DatabaseError) as error:
        app.logger.error(error)
        if conn is not None:
            cur.close()
            conn.close()

        return False
    finally:
        if conn is not None:
            cur.close()
            conn.close()


def checkBlacklist(token):
    conn = None
    try:
        conn, cur = db_connect()
        cur.execute("SELECT count(*) FROM blacklist where token=%s;", (token, ))
        result = cur.fetchone()
        if result[0] == 1:
            return True
        else:
            return False
    except (Exception, psycopg2.DatabaseError) as error:
        app.logger.error(error)
        if conn is not None:
            cur.close()
            conn.close()

        return True
    finally:
        if conn is not None:
            cur.close()
            conn.close()


def cleanBlacklist():
    conn = None
    try:
        conn, cur = db_connect()
        cur.execute("SELECT * FROM blacklist ;")
        rows = cur.fetchall()
        for row in rows:
            # Check if token in blacklist is expired and delete from database if it is
            if int(datetime.utcnow().timestamp()) > jwt.decode(row[0], options={"verify_signature": False}).get("exp"):
                cur.execute("DELETE FROM blacklist WHERE token = %s;", (str(row[0]),))
                conn.commit()
        return True
    except (Exception, psycopg2.DatabaseError) as error:
        app.logger.error(error)
        if conn is not None:
            cur.close()
            conn.close()

        return False
    finally:
        if conn is not None:
            cur.close()
            conn.close()

def db_setup():
    conn, cur = db_connect()

    # Check if table users exists
    cur.execute("select EXISTS(select * from information_schema.tables where table_name=%s);", ('users',))

    if not cur.fetchone()[0]:
        try:
            cur.execute("CREATE TABLE users (id serial PRIMARY KEY, username varchar UNIQUE, password varchar, isAdmin boolean);")
            conn.commit()
        except:
            app.logger.error("Error creating users table!")

    # Check if table roles exists
    cur.execute("select EXISTS(select * from information_schema.tables where table_name=%s);", ('roles',))

    if not cur.fetchone()[0]:
        try:
            cur.execute("CREATE TABLE roles (id serial PRIMARY KEY, user_id int NOT NULL, role varchar NOT NULL, CONSTRAINT fk_user FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE);")
            conn.commit()
        except:
            app.logger.error("Error creating roles table!")
            traceback.print_exc()

    # Check if table blacklist exists
    cur.execute("select EXISTS(select * from information_schema.tables where table_name=%s);", ('blacklist',))

    if not cur.fetchone()[0]:
        try:
            cur.execute("CREATE TABLE blacklist (token varchar PRIMARY KEY);")
            conn.commit()
        except:
            app.logger.error("Error creating blacklist table!")
            traceback.print_exc()

    cur.close()
    conn.close()
    return


def db_connect():
    # Connect to the postgres DB
    conn = psycopg2.connect(
        "dbname={0} user={1} password={2} host={3} port={4}".format(DBNAME, DBUSER, DBPASSWORD, DBHOST, DBPORT))
    cur = conn.cursor()
    #app.logger.debug("Database connection successful.")
    return conn, cur
